import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaludandoComponent } from './saludando.component';

describe('SaludandoComponent', () => {
  let component: SaludandoComponent;
  let fixture: ComponentFixture<SaludandoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaludandoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaludandoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
